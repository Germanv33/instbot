from flask import Flask, render_template, request, redirect
from InstBot import InstagramBot
from vision import theme_list_creation

app = Flask(__name__)
user = InstagramBot("", "")

@app.route('/', methods=["POST", "GET"])
def index():
    if request.method == "POST":
        user.username = request.form["Login"]
        user.password = request.form["Password"]

        result = user.login()

        if result == "We need auth key":
            return redirect("/auth")
        else:
            return redirect("/massliking")
    else:
        return render_template("index.html")


@app.route('/auth', methods=["POST", "GET"])
def auth():
    if request.method == "POST":
        auth_code = request.form["AuthCode"]
        user.auth_login(auth_key=auth_code)
        return redirect("/func")
    else:
        return render_template("auth.html")


@app.route("/massliking")
def massliking():
    return render_template("massliking.html")


@app.route("/masslikingrun")
def masslikingrun():
    theme_set = theme_list_creation(user)
    print("List has been created!")
    for theme in theme_set:
        user.like_photo_by_hashtag(theme)
    return 'Done, bro!'


@app.route("/func")
def func():
    return render_template("func.html")


if __name__ == "__main__":
    app.run(debug=True)