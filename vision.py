import os
from google.cloud import vision_v1


def theme_list_creation(user):
    theme_list = []
    userpage_url = f"https://www.instagram.com/{user.username}/"

    #user = InstagramBot(login, password)
    #user.login(auth_key)

    url_list = user.get_all_posts_urls(userpage_url)
    img_list = []
    print(url_list)

    for url in url_list:
        img_list.append(user.photo_url(url))
    print(img_list)

    for url in img_list:
        if url != None:
            theme_list += labels_detection(url)
    
    return set(theme_list)


def labels_detection(url):
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "keyFile.json"
    client = vision_v1.ImageAnnotatorClient()

    image = vision_v1.types.Image()
    image.source.image_uri = url
    features = [{"type_": vision_v1.Feature.Type.LABEL_DETECTION, "max_results": 5}]
    requests = [{"image": image, "features": features}]

    response = client.batch_annotate_images(requests=requests)
    label_list = []
    for image_response in response.responses:
        for label in image_response.label_annotations:
            print({'label': label.description, 'score': label.score})
            label_list.append(label.description)
    return label_list



